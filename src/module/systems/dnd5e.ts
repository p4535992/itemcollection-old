export default {
  ITEM_WEIGHT_ATTRIBUTE: `data.quantity`,
  ITEM_QUANTITY_ATTRIBUTE: `data.weight`,
  ITEM_CAPACITY_ATTRIBUTE: `data.capacity`,
  ITEM_CURRENCY_ATTRIBUTE: `data.currency`,
  ITEM_PRICE_ATTRIBUTE: `data.price`,
  ITEM_CURRENCY_MAP:{
    "pp" :10,
    "gp": 1,
    "ep": 0.5,
    "sp": 0.1,
    "cp": 0.01
  }
};

const CONSTANTS = {
  MODULE_NAME: 'itemcollection',
  /**
   * The Module name
   */
  name: 'itemcollection',
  /**
  * The module title
  */
  title: "Item Collection Settings",
  /**
  * Some generic path references that might be useful later in the application's windows
  */
  path: {
    root: `/modules/itemcollection/`,
    itemSideBarTemplate:`/modules/itemcollection/templates/bag-sheet.html`,
    itemDetailsTemplate:`/modules/itemcollection/templates/shop-sheet.html`,
  },
  // PATH: `/modules/itemcollection/`,
};

// CONSTANTS.PATH = `modules/${CONSTANTS.MODULE_NAME}/`;

export default CONSTANTS;
